package br.com.spin.service;

public interface Mensageiro {

	public void enviarMensagem(String mensagem);
	
}