package br.spin.cadastro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@ManagedBean
@ViewScoped
public class PerfilUsuarioBean implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String razaoSocial;
	private String cnpj;
	private String cidadeContrato;
	private Date dataContrato;
	private long valorContrato;
	private String modalidade;
	private String metodoPagamento;
	private String formaPagamento;
	
	private List<String> cidades = new ArrayList<String>();
	private List<String> formaPagamentos = new ArrayList<>();
	private List<String> metodoPagamentos = new ArrayList<>();
	
	public PerfilUsuarioBean(){
		cidades.add("Varginha");
		cidades.add("Curitiba");
		cidades.add("Rio de Janeiro");
		cidades.add("Lavras");
		cidades.add("Campinas");
		cidades.add("Itajuba");
		cidades.add("Porto Alegre");
		cidades.add("Juiz de Fora");
		cidades.add("São Lourenço");
		
		metodoPagamentos.add("Cartão de Crédito");
		metodoPagamentos.add("Cheque");
		metodoPagamentos.add("Boleto");
	}
	
	public List<String> segerirCidades(String consulta){
		List<String> cidadesSugeridos = new ArrayList<>();
		
		for (String cidade : this.cidades) {
			if (cidade.toLowerCase().startsWith(consulta.toLowerCase())) {
				cidadesSugeridos.add(cidade);
			}
		}

		return cidadesSugeridos;	
	}
	
	public void carregarFormaPagamento() {
		formaPagamentos.clear();
		
		if ("Cartão de Crédito".equals(metodoPagamento)) {
			formaPagamentos.add("Ávista");
			formaPagamentos.add("1x");
			formaPagamentos.add("2x");
			formaPagamentos.add("3x");
			formaPagamentos.add("4x");
			formaPagamentos.add("5x");
			formaPagamentos.add("6x");	
		}
		if ("Cheque".equals(metodoPagamento)) {
			formaPagamentos.add("Ávista");
			formaPagamentos.add("30dias");	
		}
		if ("Boleto".equals(metodoPagamento)) {
			formaPagamentos.add("Ávista");
		}
	}
	

	public void cadastrar() {
		System.out.println("Razão Social: "+ this.razaoSocial);
		System.out.println("CNPJ: "+ this.cnpj);
		System.out.println("Cidade do Contrato: "+ this.cidadeContrato);
		System.out.println("Modalidade: "+ this.modalidade);
		System.out.println("Data do Contrato: "+ this.dataContrato);
		System.out.println("Valor do Contrato: "+ this.valorContrato);
		System.out.println("Método de Pagamento: "+ this.metodoPagamento);
		System.out.println("Forma de Pagamento: "+ this.formaPagamento);

		System.out.println("-----------------------------------------------------");

		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Perfil Atualizado!"));
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCidadeContrato() {
		return cidadeContrato;
	}
	public void setCidadeContrato(String cidadeContrato) {
		this.cidadeContrato = cidadeContrato;
	}
	public Date getDataContrato() {
		return dataContrato;
	}
	public void setDataContrato(Date dataContrato) {
		this.dataContrato = dataContrato;
	}
	public long getValorContrato() {
		return valorContrato;
	}
	public void setValorContrato(long valorContrato) {
		this.valorContrato = valorContrato;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public List<String> getFormaPagamentos() {
		return formaPagamentos;
	}

	public void setFormaPagamentos(List<String> formaPagamentos) {
		this.formaPagamentos = formaPagamentos;
	}

	public List<String> getCidades() {
		return cidades;
	}

	public String getMetodoPagamento() {
		return metodoPagamento;
	}

	public void setMetodoPagamento(String metodoPagamento) {
		this.metodoPagamento = metodoPagamento;
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	public void setCidades(List<String> cidades) {
		this.cidades = cidades;
	}
	public List<String> getMetodoPagamentos() {
		return metodoPagamentos;
	}

	public void setMetodoPagamentos(List<String> metodoPagamentos) {
		this.metodoPagamentos = metodoPagamentos;
	}
	

}
