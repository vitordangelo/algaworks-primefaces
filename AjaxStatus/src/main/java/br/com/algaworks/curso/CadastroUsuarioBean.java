package br.com.algaworks.curso;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@ManagedBean
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private String login;

	public void cadastrar() {
		System.out.println("Login: " + this.login);
		System.out.println("Nome: " + this.nome);

		// Mensagem para o usuário na página xhtml
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Cadastro realizado!"));
	}

	public void verificarDisponibilidadeLogin() {
		FacesMessage msg = null;
		System.out.println("Verificando disponibilidade" + this.login);

		// simula processamento
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}

		if ("joao".equalsIgnoreCase(this.login)) {
			msg = new FacesMessage("Login já em uso. Escolha outro!");
			msg.setSeverity(FacesMessage.SEVERITY_WARN);
		} else {
			msg = new FacesMessage("Login disponivel!");
		}

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
