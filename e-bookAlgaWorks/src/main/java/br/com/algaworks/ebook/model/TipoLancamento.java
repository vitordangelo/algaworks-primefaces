package br.com.algaworks.ebook.model;

public enum TipoLancamento {
	RECEITA, DESPESA
}
