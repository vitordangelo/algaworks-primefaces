package br.com.algaworks.ebook.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private static EntityManagerFactory factory;
	static {
		factory = Persistence.createEntityManagerFactory("e-booksAlgaworksPU");
	}

	public static EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

}
