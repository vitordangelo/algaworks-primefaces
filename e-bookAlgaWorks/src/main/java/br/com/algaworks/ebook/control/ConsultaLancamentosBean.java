package br.com.algaworks.ebook.control;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.persistence.EntityManager;

import br.com.algaworks.ebook.model.Lancamento;
import br.com.algaworks.ebook.repository.Lancamentos;
import br.com.algaworks.ebook.util.JpaUtil;

@ManagedBean
@ViewScoped
public class ConsultaLancamentosBean {

	private List<Lancamento> lancamentos;

	public void consultar() {
		EntityManager manager = JpaUtil.getEntityManager();
		Lancamentos lancamentos = new Lancamentos(manager);
		this.lancamentos = lancamentos.todos();
		manager.close();
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

}

// No managed bean ConsultaLancamentosBean possui código de acesso a dados, e
// isso
// pode não ser interessante, principalmente em aplicações médias e grandes,
// pois não
// conseguiremos reaproveitar a lógica de acesso aos dados. Por isso, vamos
// aprender e
// implementar o padrão Repository.

// package br.com.algaworks.ebook.control;
//
// import java.util.List;
//
// import javax.faces.bean.ManagedBean;
// import javax.faces.view.ViewScoped;
// import javax.persistence.EntityManager;
// import javax.persistence.TypedQuery;
//
// import br.com.algaworks.ebook.model.Lancamento;
// import br.com.algaworks.ebook.util.JpaUtil;
//
// @ManagedBean
// @ViewScoped
// public class ConsultaLancamentosBean {
//
// private List<Lancamento> lancamentos;
//
// public void consultar() {
// EntityManager manager = JpaUtil.getEntityManager();
// TypedQuery<Lancamento> query = manager.createQuery("from Lancamento",
// Lancamento.class);
// this.lancamentos = query.getResultList();
// manager.close();
// }
//
// public List<Lancamento> getLancamentos() {
// return lancamentos;
// }
// }