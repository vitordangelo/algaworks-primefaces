import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;


@ManagedBean
@ViewScoped
public class PerfilUsuarioBean implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private List<String> estados = new ArrayList<>();
	public List<String> getEstados() {
		return estados;
	}

	public List<String> getCidades() {
		return cidades;
	}
	private List<String> cidades = new ArrayList<>();

	private String nome;
	private String cidade;
	private String estado;
		
	public void cadastrar() {
		System.out.println("Estado: "+ this.estado);
		System.out.println("Cidade: "+this.cidade);
		
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Perfil Atualizado!"));
	}
	
	public PerfilUsuarioBean(){
		estados.add("MG");
		estados.add("SP");
		estados.add("RJ");
	}
	
	public void carregarCidades() {
		cidades.clear();
		
		if ("MG".equals(estado)) {
			cidades.add("Varginha");
			cidades.add("Sta Rita");
			cidades.add("Itajuba");
		}
		
		if ("SP".equals(estado)) {
			cidades.add("SP");
			cidades.add("Compinas");
			cidades.add("Jundiai");
		}
		
		if ("RJ".equals(estado)) {
			cidades.add("RJ");
			cidades.add("Niteroi");
			cidades.add("Buzios");
		}
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
