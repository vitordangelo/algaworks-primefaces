package br.com.spin.model;

public enum TipoPessoa {

	FISICA, JURIDICA
}
