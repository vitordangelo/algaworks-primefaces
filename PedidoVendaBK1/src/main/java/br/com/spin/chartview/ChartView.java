package br.com.spin.chartview;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
 
@ManagedBean
public class ChartView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LineChartModel areaModel;
 
    @PostConstruct
    public void init() {
        createAreaModel();
    }
 
    public LineChartModel getAreaModel() {
        return areaModel;
    }
     
    private void createAreaModel() {
        areaModel = new LineChartModel();
 
        LineChartSeries temperatura = new LineChartSeries();
        temperatura.setFill(true);
        temperatura.setLabel("temperatura");
        temperatura.set("2004", 120);
        temperatura.set("2005", 100);
        temperatura.set("2006", 44);
        temperatura.set("2007", 150);
        temperatura.set("2008", 25);
 
        LineChartSeries umidade = new LineChartSeries();
        umidade.setFill(true);
        umidade.setLabel("umidade");
        umidade.set("2004", 52);
        umidade.set("2005", 60);
        umidade.set("2006", 110);
        umidade.set("2007", 90);
        umidade.set("2008", 120);
 
        areaModel.addSeries(temperatura);
        areaModel.addSeries(umidade);
         
        areaModel.setTitle("Area Chart");
        areaModel.setLegendPosition("ne");
        areaModel.setStacked(true);
        areaModel.setShowPointLabels(true);
         
        Axis xAxis = new CategoryAxis("Years");
        areaModel.getAxes().put(AxisType.X, xAxis);
        Axis yAxis = areaModel.getAxis(AxisType.Y);
        yAxis.setLabel("Births");
        yAxis.setMin(0);
        yAxis.setMax(300);
    }
     
}