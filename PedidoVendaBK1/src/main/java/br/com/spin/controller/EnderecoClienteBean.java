package br.com.spin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class EnderecoClienteBean {

	private List<Integer> enderecoFiltrados;
	
	public EnderecoClienteBean() {
		enderecoFiltrados = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			enderecoFiltrados.add(i);
		}
	}

	public List<Integer> getenderecoFiltrados() {
		return enderecoFiltrados;
	}
	
}