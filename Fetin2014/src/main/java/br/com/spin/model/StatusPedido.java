package br.com.spin.model;

public enum StatusPedido {
	ORCAMENTO, EMITIDO, CANCELADO
}
