package br.com.spin.controller;

import javax.inject.Named;

@Named("beanCDI")
public class PrecoProdutoBean {

	public double getPreco() {
		return 10.45;
	}

}