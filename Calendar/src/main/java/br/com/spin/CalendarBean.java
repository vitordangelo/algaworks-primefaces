package br.com.spin;

import java.io.Serializable;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@ManagedBean
@ViewScoped
public class CalendarBean implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private Date dataNascimento;
	
	public void cadastrar() {
		System.out.println("Data Nascimento: " + this.dataNascimento);
		
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Perfil Atualizado!"));
	}
	
	//Método para retornar a data atual
	public Date getDataHoje() {
		return new Date();
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
