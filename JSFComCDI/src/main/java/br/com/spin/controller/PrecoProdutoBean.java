package br.com.spin.controller;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.spin.sevice.CalculadoraPreco;


@Named("meuBean")
public class PrecoProdutoBean {

	@Inject
	private CalculadoraPreco calculadora;
	
	public double getPreco() {
		return calculadora.calcularPreco(12, 44.55);
	}
	
}