import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@ManagedBean
@ViewScoped
public class PerfilUsuario implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String profissao;
	
	public void cadastrar() {
		System.out.println("Profissão " + this.profissao);
		
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Perfil Atualizado!"));

	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
}
